﻿#include <iostream>
#include <stack>

int main()
{
	std::stack <int> steck;

	int i = 0;
	while (i != 5)
	{
		int num;
		std::cin >> num;
		steck.push(num);
		i++;
	}

	std::cout << "Top stack: " << steck.top() << std::endl;
	steck.pop();
	std::cout << "New top stack: " << steck.top();

}
